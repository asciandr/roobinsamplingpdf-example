# RooBinSamplingPdf usage example

Example code to show bias induced by old RooFit features in fits of unbinned models to binned datasets and how to fix it by means of the new [RooBinSamplingPdf](https://root.cern.ch/doc/master/classRooBinSamplingPdf.html) class.

```
source setup.sh
source Make.sh
```

Run exe as:
```
./run_toy_fit.exe [N_ev] [N_bins] [sig_frac] [integral_precision]
```

Run without PDF integration:
```
./run_toy_fit.exe 500000 32 0.1 0
```
![alt text](Plot_OFF.png)
![alt text](Pull_OFF.png)

Run with PDF integration:
```
./run_toy_fit.exe 500000 32 0.1 1e-3
```
![alt text](Plot_ON.png)
![alt text](Pull_ON.png)
