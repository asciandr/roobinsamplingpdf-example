///////////////// -*- C++ -*- ///////////////////
// Author: A.Sciandra<andrea.sciandra@cern.ch> //
/////////////////////////////////////////////////

// includes
// general ROOT
#include "TCanvas.h"
#include "TFile.h"

// RooFit
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooArgList.h"
#include "RooBinSamplingPdf.h"
#include "RooCategory.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooHist.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"

using namespace std;
using namespace RooFit;

// declare functions defined below

int main(int argc, char**argv) {

  if (argc < 5) {
    cout << "Correct usage: ./exe N_ev N_bins sig_frac integral_precision . Exiting..." << endl;
    exit(0);
  }

  // n. of events
  long N_ev = strtol(argv[1], NULL, 10);
  // n. of bins 
  long N_bins = strtol(argv[2], NULL, 10);
  // use RooBinSamplingPdf?
  double sig_frac = stod(argv[3]);
  // use RooBinSamplingPdf?
  double intBins = stod(argv[4]);

  // TFile to store RooFitResult
  //TFile *f_out = new TFile("f.root","RECREATE");
  // Declare observable x
  RooRealVar x("x", "M [GeV]", 70, 230);
  x.setBins(N_bins);

  // build QCD model
  RooRealVar theta1("theta1", "theta1", -0.7, -100, 100);
  RooRealVar theta2("theta2", "theta2", -0.05, -100, 100);
  RooRealVar theta3("theta3", "theta3", -0.1, -100, 100);
  RooRealVar theta4("theta4", "theta4", 0.05, -100, 100);
  RooGenericPdf bkg("bkg", "Background component", "TMath::Exp(@1*TMath::Power((@0-150.)/80.,1)+@2*TMath::Power((@0-150.)/80.,2)+@3*TMath::Power((@0-150.)/80.,3)+@4*TMath::Power((@0-150.)/80.,4))", RooArgList(x, theta1, theta2, theta3, theta4));

  // build Z model
  RooRealVar mean("mean", "mean of gaussians", 91.2, 91.2, 91.2);
  RooRealVar sigma("sigma", "width of gaussians", 8.5, 8.5, 8.5);

  RooGaussian sig("sig", "Signal component", x, mean, sigma);
  //RooGenericPdf sig("sig", "Signal component", "TMath::Gaus(@0,@1,@2)", RooArgList(x, mean, sigma));

  // combine QCD+Z into a RooSimultaneous
  // and generate workspace
  RooRealVar sigfrac("sigfrac", "Fraction of signal component", sig_frac, 0, 1);
  RooAddPdf model("model", "model", RooArgList(sig, bkg), sigfrac);

  // Manually wrap model PDF
  RooBinSamplingPdf binSampler("binSampler", "binSampler", x, model , intBins);
  // Generate a data sample of N_ev events in x from model
  RooAbsData *data = intBins ? (RooAbsData*)binSampler.generate(x, N_ev, ExpectedData(kTRUE)) : (RooAbsData*)model.generate(x, N_ev, ExpectedData(kTRUE));
  // Fit RooBinSamplingPdf-wrapped model to data
  if(intBins) 	binSampler.fitTo(*data);
  else		model.fitTo(*data);

  // Plot data and PDF overlaid
  TCanvas *c = new TCanvas("c");
  RooPlot *xframe = x.frame(Title("sig+bkg fit"));
  data->plotOn(xframe, DataError(RooAbsData::Poisson));
  if(intBins)	binSampler.plotOn(xframe);
  else		model.plotOn(xframe);
  xframe->Draw();
  c->SaveAs("plot.pdf");

  // Construct a histogram with the pulls of the data w.r.t the curve
  RooHist *hpull = xframe->pullHist();
  // Create a new frame to draw the pull distribution and add the distribution to the frame
  TCanvas *c2 = new TCanvas("c2");
  RooPlot *xframe2 = x.frame(Title("Pull Distribution"));
  xframe2->addPlotable(hpull, "P");
  xframe2->Draw();
  c2->SaveAs("pull.pdf");

  //f_out->Add(w);
  //f_out->Write();
  //f_out->Close();
  
  return 0;
}
